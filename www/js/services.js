
angular.module('starter.services', [])
.constant('SERVER','http://ibiapp.com.br')
// .constant('SERVER','http://localhost:8100/api')

 
.service('LoginService', function($q, $http, SERVER, $window, $ionicHistory) {
    return {
        loginUser: function(codigo) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            $window.localStorage['session'] = $window.localStorage['session'] || '{}'

            deferred.resolve(
                $http({async: false, url: SERVER + "/app/v2/" + codigo, method: 'GET'})
                
                    .success(function(data, status, headers, config) {
                        // console.log(data.status)

                        if (status != 200) {
                          deferred.reject('Wrong credentials.');
                        } else {
                            
                            if (data.error == undefined) {
                                deferred.resolve('Welcome!');
                                $window.localStorage['session'] = JSON.stringify(data)
                                $window.localStorage['error'] = ''
                                $window.localStorage['expire'] 

                                
                                // return true
                            } else {
                                console.log('error')
                                deferred.reject('Wrong credentials.');
                                
                                $window.localStorage.clear();
                                $ionicHistory.clearCache();
                                $window.localStorage['session'] || '{}'
                                $window.localStorage['error'] = data.error 
                                // return false
                            } 
                        }

                        // promise.success = function(fn) {
                        //     promise.then(fn);
                        //     return promise;
                        // }
                        // promise.error = function(fn) {
                        //     promise.then(null, fn);
                        //     return promise;
                        // }
                        
                        

                    })
                );
            // console.log(2222)
            return deferred.promise;
            
            

            
        },
        
        logout: function() {
            var deferred = $q.defer();
            var promise = deferred.promise;
            
            $window.localStorage.clear();
            $ionicHistory.clearCache();
            $window.localStorage['session'] = '{}'
            return promise;
    
        }
    }
})


