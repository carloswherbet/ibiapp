angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal,LoginService, $timeout, $state, $window, $location) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};
  $window.localStorage['session'] = $window.localStorage['session'] || '{}'
  var token = $window.localStorage['session'] 
  if (token == '{}' ) {

    $state.go('login');

  } else {

    
  }

  


  // Create the login modal that we will use later
  // $ionicModal.fromTemplateUrl('templates/login.html', {
  //   scope: $scope
  // }).then(function(modal) {
  //   $scope.modal = modal;
  // });

  // Triggered in the login modal to close it
  // $scope.closeLogin = function() {
  //   $scope.modal.hide();
  // };

  // Open the login modal
  $scope.logout = function() {
    LoginService.logout()
    $location.path("/login");
    // $state.go('login');
    
  };

  // Perform the login action when the user submits the login form
  // $scope.doLogin = function() {
  //   console.log('Doing login', $scope.loginData);

  //   // Simulate a login delay. Remove this and replace with your login
  //   // code if using a login system
  //   $timeout(function() {
  //     $scope.closeLogin();
  //   }, 1000);
  // };
})

.controller('ArquivosCtrl', function($scope, $ionicHistory, $window) {
  // $ionicHistory.nextViewOptions({
  //   disableBack: true
  // });
  // $ionicHistory.clearCache();
  // 
  
  var session = JSON.parse($window.localStorage['session'])
  // var vendedor = JSON.parse($window.localStorage['vendedor'])
  console.log(session)
  $scope.arquivos = []
  $scope.vendedor = session.vendedor.code +' - '+ session.vendedor.name
  // console.log($window.localStorage['vendedor'])
  $scope.filial = session.filial.name 
  angular.forEach(session.file, function(value, key) {
    $scope.arquivos.push({title: value['name'], path: value['path'], image: value['image'], id: value['name']})
    })

})
.controller('LoginCtrl', function($location,$scope, $ionicPopup, LoginService, $state, $window, SERVER, $ionicLoading, $timeout, $window) {
  $scope.data = {};
  
  var token = $window.localStorage['session']
  if (token != '{}' ) {
    $state.go('app.arquivos');
  } 
  
  $scope.login = function() {
      $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });

      LoginService.loginUser($scope.data.codigo).then(function(data) {
          // $state.go('app.arquivos');
          console.log(data)
          
          if ($window.localStorage['error'] == '') {
            
            $location.path("/app/arquivos");

            $scope.data.codigo = ''

          } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Folha no Login!',
                template: 'Código Invalido'
            });
          }
          
        
      })
      
      $timeout(function () {
          $ionicLoading.hide();
      }, 1000);
  }

  // $scope.data = {};
  
  //  $scope.login = function() {
  //      console.log("LOGIN user: " + $scope.data.username + " - PW: " + $scope.data.password);
  //  }
})

.controller('DownloadCtrl', function($scope, $stateParams, SERVER, $window) {
  var session = JSON.parse($window.localStorage['session'])

$scope.arquivos = []
  angular.forEach(session.file, function(value, key) {
    $scope.arquivos.push({title: value['name'], path: value['path'], image: value['image'], id: value['name']})
  })

  $scope.exturl = function(item) {
    console.log("TAP!");
    var url = item;
    console.log(url);
    var ref = $window.open(url, '_system', 'location=yes');
    // var ref = window.open(url,"_system","location=yes,enableViewportScale=yes,hidden=no");
   };


  angular.forEach($scope.arquivos, function(value, key) {
          
            if (value['id'] == $stateParams.arquivoId) {
                $scope.nome = value['title']
                $scope.image = SERVER + value['image']
                $scope.path_pdf = SERVER + value['path']
            }

        })
});
